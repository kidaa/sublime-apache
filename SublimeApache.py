import sublime, sublime_plugin

class SublimeApacheCommand(sublime_plugin.WindowCommand):
    def run(self, *args, **kwargs):
        settings_file = 'SublimeApache.sublime-settings'

        self.settings = sublime.load_settings(settings_file)
        self.bin_dir = ''
        self.command = kwargs.get('command', None)

        if self.command != None:
            self.bin_dir = self.settings.get('bin_dir', '')

            if len(self.bin_dir) and self.bin_dir[-1:] != '/':
                self.bin_dir += '/'

            command_flags = {
                'start': '-k',
                'stop': '-k',
                'restart': '-k'
            }

            if self.command in command_flags:
                self.command = command_flags[self.command] + self.command

            self.run_command()

    def run_command(self):
        cmd = [self.bin_dir + 'httpd', self.command]

        self.window.run_command('exec', {
            'cmd': cmd,
            'shell': True
        })
