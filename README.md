# Sublime Apache
Sublime Apache is a simple plugin that provides a way to interact with
Apache via Sublime Text 2 and 3.

To use, just open the command palette and type in "Apache:".
You should then be able to see a list of available commands.

## Notes
Currently this plugin has only been tested on Windows.
